import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import {createStackNavigator} from 'react-navigation';

import HomeScreen from './src/screens/HomeScreen';
import PullRequestScreen from './src/screens/PullRequestScreen';

export default class App extends React.Component {
   
  render() {
    return (
      <AppStackNavigator />
    );
  }
}

const AppStackNavigator = createStackNavigator({
  Home: HomeScreen,
  PullRequest: PullRequestScreen

})
