import React from 'react';
import { View, FlatList, StyleSheet, Text } from 'react-native';
import CustomRow from './CustomRow';


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

const CustomListview = ({ itemList }) => (
    <View style={styles.container}>
        <FlatList
                data={itemList}
                renderItem={({ item }) => <CustomRow
                    title={item.name}
                    description={item.description}
                    image_url={item.avatar_url}
                    login = {item.login}
                    stargazers_count = {item.stargazers_count}
                />}
            />

    </View>
);
export default CustomListview;