import React from 'react';
import { StyleSheet, FlatList, Text, View, Button, Image} from 'react-native';
import { Icon } from 'react-native-elements';
import api from '../services/api';

class HomeScreen extends React.Component {

    static navigationOptions = ({navigation}) => ({
        title: 'Popular Java Repositories'
    });

    state = {
        respositorios: []
    }

    componentWillMount() {
        api.get('/search/repositories?q=Java+language:java&sort=stars&order=desc')
        .then((response) => this.setState({ repositorios: response.data.items }))
    }
  
    render() {
    
        return (
            <View style={styles.container}>
                <FlatList
                    data={this.state.repositorios}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => (
                        <View style={styles.container}>
                        <Image source={{ uri: item.owner.avatar_url }} style={styles.photo} />
                        <View style={styles.container_text}>
                            <Text style={styles.title}>
                                {item.name}
                            </Text>
                            <Text style={styles.owner_login}>
                            {item.owner.login}
                            </Text>
                            <Text style={styles.description}>
                            {item.description}
                            </Text>
                            <View style={{flexDirection: 'row'}} >
                                <Icon name='star' type='ant-design' color='#517fa4' iconStyle={styles.other_infos} />
                                <Text style={styles.other_infos}> {item.stargazers_count} </Text>
                            </View>
                            <View style={{flexDirection: 'row'}} >
                                <Icon name='code-fork' type='font-awesome' color='#517fa4' />
                                <Text style={styles.other_infos}> {item.forks_count} </Text>
                            </View>
                            <Button title="See Pull Requests"
                            onPress={()=> this.props.navigation.navigate('PullRequest', {pulls_url: (item.pulls_url).slice(22,-9)})} />
                        </View>
                    </View>  
                 )}
               />
            </View>
        );
    }
}

export default HomeScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
        marginLeft:16,
        marginRight:16,
        marginTop: 8,
        marginBottom: 8,
        borderRadius: 5,
        backgroundColor: '#FFF',
        elevation: 2,
    },

    title: {
        fontSize: 20,
        color: '#000',
    },
    container_text: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 12,
        justifyContent: 'center',
    },
    description: {
        marginTop: 12,
        marginBottom: 12,
        fontSize: 15,
        fontStyle: 'italic',
        textAlign: 'justify'
    },
    owner_login:{
        fontSize: 15,
        fontWeight: 'bold'
    },
    other_infos: {
       marginTop: 2,
       marginBottom: 16,
       fontSize: 16,        
    },
    photo: {
        height: 50,
        width: 50,
    },
  });
  
