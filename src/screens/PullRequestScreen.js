import React, {Component} from 'react';
import { StyleSheet, FlatList, Text, View, Button, Image, Linking } from 'react-native';
import { Icon } from 'react-native-elements';
import api from '../services/api';

class PullRequestScreen extends React.Component {

    static navigationOptions = ({navigation}) => ({
        title: 'Pull Requests List'
    });

    state = {
        registros: [],
    }

    componentWillMount() {
       api.get(this.props.navigation.state.params.pulls_url)
        .then((response) => this.setState({ registros: response.data }))
    }
     
  render() {
    
    return (
        <View style={styles.container}>
            <FlatList
                data={this.state.registros}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                    <View style={styles.container}>
                    <Image source={{ uri: item.user.avatar_url }} style={styles.photo} />
                    <View style={styles.container_text}>
                        <Text style={styles.title}>
                            {item.title}
                        </Text>
                        <Text style={styles.owner_login}>
                           {item.user.login}
                        </Text>
                        <Text style={styles.description}>
                           {item.body}
                        </Text>
                        <View style={{flexDirection: 'row'}} >
                            <Icon name='calendar-plus-o' type='font-awesome' color='#517fa4' />
                            <Text style={styles.other_infos}> {item.created_at} </Text>
                        </View>
                       <Button title="See Details"
                        onPress={()=> Linking.openURL(item.html_url)} />
                    </View>
                </View>   
                )}
            />
        </View>
    );
    
    return (
      <View style={styles.container} >
        <Text> PullRequestScreen - {JSON.stringify(pulls_url)} </Text>
      </View>
    );
  }
}

export default PullRequestScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
        marginLeft:16,
        marginRight:16,
        marginTop: 8,
        marginBottom: 8,
        borderRadius: 5,
        backgroundColor: '#FFF',
        elevation: 2,
    },

    title: {
        fontSize: 20,
        color: '#000',
    },
    container_text: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 12,
        justifyContent: 'center',
    },
    description: {
        marginTop: 8,
        marginBottom: 8,
        fontSize: 15,
        fontStyle: 'italic',
        textAlign: 'justify'
    },
    owner_login:{
        fontSize: 15,
        fontWeight: 'bold'
    },
    other_infos: {
      marginTop: 12,
      marginBottom: 12,
      fontSize: 16,        
    },
    photo: {
        height: 50,
        width: 50,
    },
  });
  
